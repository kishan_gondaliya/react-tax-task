import logo from "./logo.svg";
import "./App.css";
import React, { useState, useEffect } from "react";

import { useFormik } from "formik";
import Chack from "./check-icn.svg";
import {data} from './data'

const validate = (values) => {
  const errors = {};

  if (!values.firstName) {
    errors.firstName = "Required";
  } else if (values.firstName.length > 15) {
    errors.firstName = "Must be 15 characters or less";
  }
  if (!values.percentage) {
    errors.percentage = "Required";
  }

  return errors;
};

function App() {
  const userData = [
    { name: "jasinthe bracelet" },
    { name: "jasinthe bracele" },
    { name: "inspire bracelet" },
  ];


  const userObj = [
    { name: "Zero amount item with questions" },
    {
      name: "normal item with questions",
    },
    { name: "normal item" },
  ];

  const [users, setUsers] = useState([]);
  const [question, setQuestions] = useState([]);
  const [rValues, setValues] = useState("");
  useEffect(() => {
    setUsers(userData);
    setQuestions(userObj);
  }, []);

  const handleChanges = (e) => {
    const { name, checked } = e.target;
    if (name === "allSelect") {
      let tempUser = users.map((user) => {
        return { ...user, isChecked: checked };
      });
      setUsers(tempUser);
    } else {
      let tempUser = users.map((user) =>
        user.name === name ? { ...user, isChecked: checked } : user
      );
      setUsers(tempUser);
    }
  };

  const handleonChange = (e) => {
    const { name, checked } = e.target;
    if (name === "all") {
      let tempUser = question.map((user) => {
        return { ...user, isChecked: checked };
      });
      setQuestions(tempUser);
    } else {
      let tempUser = question.map((user) =>
        user.name === name ? { ...user, isChecked: checked } : user
      );
      setQuestions(tempUser);
    }
  };

  const formik = useFormik({
    initialValues: {
      firstName: "test",
      percentage: "4",
    },
    validate,
    onSubmit: (values) => {
      console.log(values);
      console.log(users);
      console.log(question);
      console.log(rValues);
    },
  });

  const onChangeButton = (e) => {
    const { name, value } = e.target;

    setValues(value);
  };

  return (
    <div className="app" style={{ marginTop: 30 }}>
      <div style={{ marginLeft: 20 }}>Add Tax</div>
      <form onSubmit={formik.handleSubmit}>
        <div
          class="form-row align-items-center"
          style={{ marginLeft: 30, marginTop: 20 }}
        >
          <div class="form-group col-md-8 mb-2">
            <input
              class="form-control"
              id="firstName"
              name="firstName"
              type="text"
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              value={formik.values.firstName}
            />
            <div className="text-danger">
              {formik.touched.firstName && formik.errors.firstName ? (
                <div>{formik.errors.firstName}</div>
              ) : null}
            </div>
          </div>
          <div class="col-md-4">
            <div class="input-group mb-2">
              <input
                type="text"
                class="form-control"
                name="percentage"
                id="percentage"
                type="text"
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.percentage}
              />
              <div class="input-group-prepend">
                <div class="input-group-text">%</div>
              </div>
            </div>
            <div className="text-danger">
              {formik.touched.percentage && formik.errors.percentage ? (
                <div>{formik.errors.percentage}</div>
              ) : null}
            </div>
          </div>
          <div className="col-12">
            <div class="custom-radios">
              <div>
                <input
                  type="radio"
                  id="color-1"
                  name="collection"
                  value="collection"
                  onChange={onChangeButton}
                />
                <label for="color-1">
                  <span>
                    <img
                      src={Chack}
                      style={{ height: 12, width: 12 }}
                      alt="Checked Icon"
                    />
                  </span>
                  Apply to all items in collection
                </label>
              </div>
            </div>
          </div>
          <div className="col-12">
            <div class="custom-radios">
              <div>
                <input
                  type="radio"
                  id="color-2"
                  name="collection"
                  value="items"
                  onChange={onChangeButton}
                />
                <label for="color-2">
                  <span>
                    <img
                      src={Chack}
                      style={{ height: 12, width: 12 }}
                      alt="Checked Icon"
                    />
                  </span>
                  Apply to specific items
                </label>
              </div>
            </div>
          </div>
          <div className="col-12">
            <div style={{ marginTop: 10 }} class="input-group mb-6 mb-2 checkbox_wrap">
              <div class="input-group-prepend">
                <div class="input-group-text custom-control custom-checkbox">
                  <input
                    type="checkbox"
                    name="allSelect"
                    aria-label="Checkbox for following text input"
                    checked={!users.some((user) => user?.isChecked !== true)}
                    onChange={handleChanges}
                    className="custom-control-input"
                    id="select1"
                  />
                  <label for="select1" className="custom-control-label"></label>
                </div>
              </div>
              <div
                style={{ background: "#e9ecef" }}
                type="text"
                class="form-control"
                aria-label="Text input with checkbox"
              >
                {" "}
                Bracelets
              </div>
            </div>
          </div>
          <div className="col-12 ml-4">
            {users.map((user) => (
              <div class="custom-control custom-checkbox multi_select">
                <input
                  class="form-check-input"
                  type="checkbox"
                  className="custom-control-input"
                  name={user.name}
                  checked={user?.isChecked || false}
                  onChange={handleChanges}
                  id={user.name}
                />
                <label for={user.name} class="custom-control-label">
                  {user.name}
                </label>
              </div>
            ))}
          </div>
          <div className="col-12">
            <div style={{ marginTop: 10 }} class="input-group mb-6 mb-2 checkbox_wrap">
              <div class="input-group-prepend">
                <div class="input-group-text custom-control custom-checkbox">
                  <input
                    type="checkbox"
                    name="all"
                    aria-label="Checkbox for following text input"
                    checked={!question.some((user) => user?.isChecked !== true)}
                    onChange={handleonChange}
                    className="custom-control-input"
                    id="select2"
                  />
                  <label for="select2" className="custom-control-label"></label>
                </div>
              </div>
              <div
                style={{ background: "#e9ecef" }}
                type="text"
                class="form-control"
                aria-label="Text input with checkbox"
              >
                {" "}
              </div>
            </div>
          </div>
          <div className="col-12 ml-4">
            {question.map((user) => (
              <div class="custom-control custom-checkbox multi_select">
                <input
                  class="custom-control-input"
                  type="checkbox"
                  className="custom-control-input"
                  name={user.name}
                  checked={user?.isChecked || false}
                  onChange={handleonChange}
                  id={user.name}
                />
                <label for={user.name} class="custom-control-label">
                  {user.name}
                </label>
              </div>
            ))}
          </div>
          <div className="col-12 text-right mt-4"> 
          <button className="btn btn-primary" style={{backgroundColor:'#e88344ec',borderColor:'#e88344ec'}} type="submit">
            Apply text to (6) items
          </button>
          </div>
        </div>
      </form>
    </div>
  );
}

export default App;
